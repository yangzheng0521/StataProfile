
*--------------------------
* 连玉君的 profile.do 文档
*--------------------------

* arlionn@163.com
* V1: 2021/1/15 11:41
* V2: 2021/4/6  14:40

* 连享会·主页：https://www.lianxh.cn



*-说明：
* 此文件设定了每次启动 stata 时需要做的一些基本设定
* 你可以在此文件中添加你希望在stata启动时立刻执行的命令
* 你也可以自行修改相关设定
* 更多相关设定： help set



*-不要自动更新
  set update_query  off  // on

*-系统参数设定			
  set type double           // 设定 generate 命令产生的新变量为双精度类型
  set matsize 800           // 设定矩阵的维度为 800x800, 可以修改，上限为 11000
  set scrollbufsize 2000000 // 结果窗口中显示的行数上限
  set more off, perma       // 关闭分页提示符
  
  *-结果显示格式
  set cformat  %4.3f  //回归结果中系数的显示格式
  set pformat  %4.3f  //回归结果中 p 值的显示格式      
  set sformat  %4.2f  //回归结果中 se值的显示格式     

  *-有点危险的设定 (2020/2/11 9:41)
  *-目前来看，这些设定只会影响 -xtabond-, -xtdpd- 命令的执行
* set showbaselevels on, permanently
* set showemptycells on, permanently  // 2020/2/11 9:33 请务必删除/关闭这条设定，否则会导致 xtabond 和 xtdpd 命令报错
* set showomitted off, permanently    // 2020/2/11 9:33 请务必删除/关闭这条设定，否则会导致 xtabond 和 xtdpd 命令报错
  set fvlabel on, permanently


*-文件路径设定：外部命令的存放位置

*  Note-有关这一部分的完整设定命令，请输入 help set 命令进行查看

  sysdir set PLUS "`c(sysdir_stata)'ado/plus"         // 外部命令的存放位置
  sysdir set PERSONAL "`c(sysdir_stata)'ado/personal" // 个人文件夹位置
  
  *采用相似的方式，可添加其它允许stata搜索的目录
  adopath + "`c(sysdir_stata)'/ado/personal/_myado"
  *adopath + "路径2" //自行添加的放置其他外部命令的路径
  adopath + "D:/myado"  // 添加其他放置 adofiles 的路径


* log文件：自动以当前日期为名存放于 stata15/do 文件夹下
* 若 stata1x/ 下没有 do 文件夹，则本程序会自动建立一个 
  cap cd `c(sysdir_stata)'do
  if _rc{
     mkdir `c(sysdir_stata)'do  //检测后发现无 do 文件夹，则自行建立一个
  }
  
  local fn = subinstr("`c(current_time)'",":","-",2)
  local fn1 = subinstr("`c(current_date)'"," ","",3)
  log    using `c(sysdir_stata)'do/log-`fn1'-`fn'.log, text replace
  cmdlog using `c(sysdir_stata)'do/cmd-`fn1'-`fn'.log, replace


  
*-stata启动后自动进入该目录 (自行设定)

    cd "`c(sysdir_personal)'" 
  * cd "D:/mypaper/paper1"    // 设定你最近经常打开的路径

  
*------------------
*- 网址导航
*------------------
  
* dis in w _n(3)  // 空三行

*-常逛网址
 
  dis in w _n "   "
  
  dis _n in w _col(10) _dup(45) "="
  dis    in w _col(10) _n _skip(20) "Hello World! Hello Stata!" _n
  dis    in w _col(10) _dup(45) "=" _n 
  
  cap songbl 
  if _rc==0{
     songbl all
	 dis in w _n(1) as error _col(10) _dup(45) "-" 
	 songbl 
  }
  else{
     cap ssc install songbl, replace
	 if _rc==0{
	    songbl all
		dis in w _n(1) as error _col(10) _dup(45) "-" 
	    songbl 
	 }
  }
  
  dis _n(2) as text " "
  
  cap lianxh
  if _rc{
     ssc install lianxh, replace
  }

  dis as error _n(1)  _col(10) _dup(46) "-" 
  dis as input _n(0)  _col(11) `"Stata 资源搜索： {stata "help lianxh"}"' _skip(5) `"{stata "help songbl"}"'
  dis as error _n(0)  _col(10) _dup(46) "-" 


  